/* global setupCountdown */

function setupHackathonCountdown() {
  var nextHackathonDate = new Date('February 14, 2019 00:00:00').getTime();

  setupCountdown(nextHackathonDate, 'nextHackathonCountdown');
}

(function() {
  setupHackathonCountdown();
})();

