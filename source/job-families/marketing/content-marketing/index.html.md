---
layout: job_family_page
title: "Content Marketer"
---

You are highly creative, self-motivated, and love to write and create content. 
You have experience building and engaging audiences across a variety of channels 
and content formats, and employ empathy and respect for your audiences. You are 
able to put yourself in the shoes of others in order to understand their needs, 
pain points, preferences, and ambitions and translate that into multimedia experiences. 
Additionally, you are excited about experimentation and analyzing the results.

### Responsibilities

- Manage content production from start to finish.
- Research and write blogs and articles on technical topics. 
- Create content for lead generation, including blog articles, infographics, eBooks, web pages, white papers, webinars, and reports.  
- Tailor content to buyer personas and segments.  
- Work with subject matter experts as needed to gather source material and technical expertise.  
- Contribute to the strategy and planning of content marketing initatives. 
- Develop a deep understanding of our corporate and product messaging 
- Translate messaging into engaging narratives and multimedia experiences.
- Work with internal subject matter experts and thought leaders to amplify their reach and influence.
- Copywriting and copyediting as needed. 

### Requirements

- Excellent writer and researcher with a strong ability to grasp new concepts quickly.
- Degree in marketing, journalism, communications or related field.
- Strong communication skills without a fear of over communication. 
- Familiarity with the content pillar concept. 
- Extremely detail-oriented and organized, able to meet deadlines.
- You share our [values](/handbook/values/), and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission.

## Levels 

### Content Marketing Associate 

- Execute content plan set forth my the Manager of Content Marketing.
- Writes multiple blog articles per week.
- Synthesize messaging and expertise from Product Marketing into accurate and compelling content. 
- Participate in pitching articles and creative content ideas. 
- Contribute results to content marketing initiatives and OKRs. 

### Requirements

- 1-3 years experience in a content-related role.
- Proven ability to write high-quality, long-form content on complex topics. 
- Able to coordinate with multiple stakeholders and perform in a fast-moving start-up environment. 
- Proven ability to work on multiple projects at at ime. 

### Content Marketing Manager

- Propose and execute on a content plan. 
- Contribute ideas and solutions beyond existing plans.  
- Write multiple blog articles per week. 
- Strategically translate messaging into accurate, targeted, and compelling content. 
- Makes decisions on, and serve as DRI for, content marketing projects and initiatives. 
- Independently manage projects from start to finish. 
- Influence content marketing initiatives and OKRs, and drives results. 

### Requirements

- 3-5 years experience in a content-related role.
- Enterprise software marketing experience.
- Proven ability to research and write on technical topics independently. 
- Proven abliity to perform a content gap analysis and execute on a content plan independently. 

### Senior Content Marketing Manager

- Participate in the development of the content marketing strategy. 
- Independently develop and execute a content plan.
- Expertly and independently communicate product messaging into accurate, targeted, and compelling content. 
- Collaborate with Product Marketing on messaging and positioning. 
- Write weekly thought leadership articles. 
- Propose, implement, or lead improvements to content marketing strategy, plan, and process.
- Mentor other members of the content marketing team. 
- Entrust work to other members of the content marketing team as appropriate. 
- Propose new content marketing tactics, opportunities, and audiences. 
- Responsible for ideation of content marketing initiatives, OKRs, and reporting on results. 

### Requirements

- 5+ years in a content-related role 
- 3+ years of enterprise software marketing experience.
- Proven experience writing for technical audiences on one or more of the following topics: Coding, DevOps, Cloud Computing, IT Infrastructure, Cyber Security, or Application Security. 
- In-depth industry experience and knowledge in a specialty. 
- Proven ability to identify new audiences and opportunities, create a strategy, and execute the strategy. 

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### Development (Dev)

The Dev specialty maps to the [Dev department](/handbook/product/categories/#dev-department) at GitLab. This specialty 
covers the Manage, Plan, and Create stages of the [DevOps lifecycle](/stages-devops-lifecycle/). As a Dev specialist,
you are responsible for covering topics that fall under these stages. For example: Workflow policies, project management, value stream management, code review, and source code management.

### Operations (Ops)

The Ops specialty maps to the [Ops department](/handbook/product/categories/#ops-department) at GitLab. This specialty 
covers the Verify, Package, Release, Configure, and Monitor stages of the [DevOps lifecycle](/stages-devops-lifecycle/). As an Ops specialist,
you are responsible for covering topics that fall under these stages. For example: Continuous integration, continuous delivery, application performance management, PaaS, serverless, and Kubernetes.

### Security (Sec)

The Security specialty maps to the [Sec department](/handbook/product/categories/#sec-department) at GitLab. This specialty 
covers the Secure and Defend stages of the [DevOps lifecycle](/stages-devops-lifecycle/). As a Security specialist,
you are responsible for covering topics that fall under these stages. For example: Application security, SAST, DAST, container security, open source security, and continuous security testing.

### Cloud Native 

The Cloud Native Content Associate will work with the Director of Cloud Native Alliances to build and execute on our cloud native thought leadership strategy.  This hire will have the opportunity to learn about two organizations at close quarters - Alliances and Marketing. Career development is critical for each employee and the Cloud Native Content Associate will be enabled to pursue their goals in whichever organization they want to grow into for the long term.

#### Responsibilities

- Develop Cloud Native Event & CFP Calendar keeping Marketing in the loop
- Create and Project Manage a Cloud Native Editoral Calendar
- Schedule and Enable Cloud Native webinars
- Represent GitLab at the CNCF Marketing Committee
- Support the Director of Cloud Native Alliances in a monthly oped contribution
- Build ROI measurement system to measure impact of activities

#### Requirements

- 2 years or more experience
- Excellent project management, organization and communication skills
- Experience coordinating across multiple departments/teams
- Experience in the DevOps, Cloud Computing space
- Bonus: Experience working with CNCF
- Bonus: Some technical education, self learning or work experience

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Candidates will submit 2-3 writing samples. 
- Next, candidates will be invited to 3 45-minute interviews: first with our Manager of Content Marketing, then a team interview, and finally with a Senior Product Marketing Manager.
- Candidates will then be invited to schedule a 45-minute interview with our CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
